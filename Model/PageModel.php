<?php

class PageModel
{

    private $pagesContent = [
        ['id' => 1, 'title' => "Главная", 'name' => "Главная", 'content' => "Контент главной страницы"],
        ['id' => 2, 'title' => "О нас", 'name' => "О нас", 'content' => "Контент О нас"],
        ['id' => 3, 'title' => "Контакты", 'name' => "Контакты", 'content' => <<<HTML
                                                                                        <section class="ftco-section contact-section px-md-4">
                                                                                    <div class="container">
                                                                                        <div class="row d-flex mb-5 contact-info">
                                                                                        <div class="col-md-12 mb-4">
                                                                                            <h2 class="h3">Contact Information</h2>
                                                                                        </div>
                                                                                        <div class="w-100"></div>
                                                                                        <div class="col-lg-6 col-xl-3 d-flex mb-4">
                                                                                            <div class="info bg-light p-4">
                                                                                                <p><span>Address:</span> 198 West 21th Street, Suite 721 New York NY 10016</p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-lg-6 col-xl-3 d-flex mb-4">
                                                                                            <div class="info bg-light p-4">
                                                                                                <p><span>Phone:</span> <a href="tel://1234567920">+ 1235 2355 98</a></p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-lg-6 col-xl-3 d-flex mb-4">
                                                                                            <div class="info bg-light p-4">
                                                                                                <p><span>Email:</span> <a href="mailto:info@yoursite.com">info@yoursite.com</a></p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-lg-6 col-xl-3 d-flex mb-4">
                                                                                            <div class="info bg-light p-4">
                                                                                                <p><span>Website</span> <a href="#">yoursite.com</a></p>
                                                                                            </div>
                                                                                        </div>
                                                                                        </div>
                                                                                        <div class="row block-9">
                                                                                        <div class="col-lg-6 d-flex">
                                                                                            <form action="#" class="bg-light p-5 contact-form">
                                                                                            <div class="form-group">
                                                                                                <input type="text" class="form-control" placeholder="Your Name">
                                                                                            <div data-lastpass-icon-root="" style="position: relative !important; height: 0px !important; width: 0px !important; float: left !important;"></div></div>
                                                                                            <div class="form-group">
                                                                                                <input type="text" class="form-control" placeholder="Your Email">
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <input type="text" class="form-control" placeholder="Subject">
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <textarea name="" id="" cols="30" rows="7" class="form-control" placeholder="Message"></textarea>
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <input type="submit" value="Send Message" class="btn btn-primary py-3 px-5">
                                                                                            </div>
                                                                                            </form>
                                                                                        
                                                                                        </div>

                                                                                        <div class="col-lg-6 d-flex">
                                                                                            <div id="map" class="bg-light" style="position: relative; overflow: hidden;"><div style="height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; background-color: rgb(229, 227, 223);"><div class="gm-err-container"><div class="gm-err-content"><div class="gm-err-icon"><img src="https://maps.gstatic.com/mapfiles/api-3/public/images/icon_error.png" alt="" draggable="false" style="user-select: none;"></div><div class="gm-err-title">Oops! Something went wrong.</div><div class="gm-err-message">This page didn't load Google Maps correctly. See the JavaScript console for technical details.</div></div></div></div></div>
                                                                                        </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    </section>                                                                                
HTML
    ]
];


    public function get($id = null)
    {
        if ($id) {
            foreach ($this->pagesContent as $page) {
                if ($page['id'] == $id) {
                    return $page;
                }
            }
        }
    }
}
