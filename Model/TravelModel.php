<?php

class TravelModel
{
    private $news = [
        [
            "id" => 1,
            "title" => "A Loving Heart is the Truest Wisdom",
            "img" => "image_1.jpg",
            "category" => "Travel",
            "description" => "A small river named Duden flows by their place and supplies it with the necessary regelialia.",
            "content" => "asd",
        ],
        [
            "id" => 2,
            "title" => "Great Things Never Came from Comfort Zone",
            "img" => "image_2.jpg",
            "category" => "Travel",
            "description" => "A small river named Duden flows by their place and supplies it with the necessary regelialia.",
            "content" => "",
        ],
        [
            "id" => 3, 
            "title" => "Paths Are Made by Walking",
            "img" => "image_3.jpg",
            "category" => "Lifestyle",
            "description" => "A small river named Duden flows by their place and supplies it with the necessary regelialia.",
            "content" => "",
        ],
        [
            "id" => 4, 
            "title" => "The Secret of Getting Ahead is Getting Started",
            "img" => "image_4.jpg",
            "category" => "Nature",
            "description" => "A small river named Duden flows by their place and supplies it with the necessary regelialia.",
            "content" => "",
        ],
        [
            "id" => 5,
            "title" => "You Can't Blame Gravity for Falling in Love",
            "img" => "image_5.jpg",
            "category" => "Lifestyle",
            "description" => "A small river named Duden flows by their place and supplies it with the necessary regelialia.",
            "content" => "",
        ],
        [
            "id" => 6, 
            "title" => "You Can't Blame Gravity for Falling in Love",
            "img" => "image_6.jpg",
            "category" => "Travel",
            "description" => "A small river named Duden flows by their place and supplies it with the necessary regelialia.",
            "content" => "",
        ],
        [
            "id" => 7, 
            "title" => "You Can't Blame Gravity for Falling in Love",
            "img" => "image_7.jpg",
            "category" => "Nature",
            "description" => "A small river named Duden flows by their place and supplies it with the necessary regelialia.",
            "content" => "",
        ],
        [
            "id" => 8, 
            "title" => "You Can't Blame Gravity for Falling in Love",
            "img" => "image_8.jpg",
            "category" => "Travel",
            "description" => "A small river named Duden flows by their place and supplies it with the necessary regelialia.",
            "content" => "",
        ],
        [
            "id" => 9, 
            "title" => "You Can't Blame Gravity for Falling in Love",
            "img" => "image_9.jpg",
            "category" => "Lifestyle",
            "description" => "A small river named Duden flows by their place and supplies it with the necessary regelialia.",
            "content" => "",
        ],
        [
            "id" => 10, 
            "title" => "You Can't Blame Gravity for Falling in Love",
            "img" => "image_10.jpg",
            "category" => "Nature",
            "description" => "A small river named Duden flows by their place and supplies it with the necessary regelialia.",
            "content" => "",
        ],
        [
            "id" => 11, 
            "title" => "You Can't Blame Gravity for Falling in Love",
            "img" => "image_11.jpg",
            "category" => "Travel",
            "description" => "A small river named Duden flows by their place and supplies it with the necessary regelialia.",
            "content" => "",
        ],
        [
            "id" => 12, 
            "title" => "You Can't Blame Gravity for Falling in Love",
            "img" => "image_12.jpg",
            "category" => "Lifestyle",
            "description" => "A small river named Duden flows by their place and supplies it with the necessary regelialia.",
            "content" => "",
        ]
    ];

    public function get($id = null)
    {
        if ($id) {
            foreach ($this->news as $new) {
                if ($new['id'] == $id) {
                    return $new;
                }
            }
        }
        return $this->news;
    }

}

